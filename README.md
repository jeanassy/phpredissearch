# PHP Search Engine

## Table of contents
* [General info](#general-information)
* [Technologies](#technologies)
* [Setup](#setup)
* [Run](#run)
* [Notes](#notes)


## General infomation

A simple PHP service that serves as a full-text search engine

It was created for a challenge requested by Simplesurance

It serves as a REST API

## Technologies

Project is created with:
* Symfony version: 4.4 (LTS)

## Setup

- To install the project, run `composer install` in the project's root directory

- To start the project, run `php bin/console server:start` in the project's root directory

## Run

- To add a document, run a JSON POST request on /document/xxx (where xxx is the id you want to give to the document, and include the text in the request body field called "content" )

- To fetch a document that was added, run a JSON GET request on /document/xxx

- To delete a document that was added, run a JSON DELETE request on /document/xxx

- To search documents (one or multiple keywords), run a JSON GET request on /search?q=keyword1 keyword2 keyword3...

## Notes

- You can replace the redis server parameters in the .env file depending on your machine's configuration