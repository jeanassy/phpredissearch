<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\Cache\Adapter\RedisAdapter;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use App\Services\RedisService;

class DocumentsController extends AbstractFOSRestController
{

    /**
     * @var RedisAdapter
     */
    private $redisService;

    /**
     * @param RedisService $redisService
     */
    public function __construct(RedisService $redisService)
    {
        $this->redisService = $redisService;
    }

    /** 
     * @Rest\Get("/search", name="documents_search")
     * @Route("/", name="default")
     * 
     * @param Request $request
     * @return Response
     */
    public function index(Request $request): Response
    {
        $keywords = $request->get("q");

        $view = $this->view($this->redisService->search($keywords), Response::HTTP_OK);
        return $this->handleView($view);
    }

    /** 
     * @Rest\Get("/document/{id}", name="documents_read") 
     * 
     * @param int $id
     * @return Response
     */
    public function readDocument(int $id): Response
    {
        $content = $this->redisService->read($id);

        if (!$content) {
            throw new NotFoundHttpException(sprintf(
                'The resource \'%s\' was not found.',
                $id
            ));
        }

        $view = $this->view($content, Response::HTTP_OK);
        return $this->handleView($view);
    }


    /** 
     * @Rest\Post("/document/{id}", name="documents_new") 
     * 
     * @param Request $request
     * @param int $id
     * 
     * @return Response
     */
    public function newDocument(Request $request, int $id): Response
    {
        $params = json_decode($request->getContent(), true);

        $content = $params['content'];

        if (strlen($content) < 1) {
            throw new BadRequestHttpException(sprintf(
                'Content is empty',

            ));
        }

        $view = $this->view($this->redisService->create($id, $content), 200);
        return $this->handleView($view);
    }


    /** 
     * @Rest\Delete("/document/{id}", name="documents_delete") 
     * 
     * @param int $id
     * @return Response
     */
    public function deleteDocument(int $id): Response
    {

        $response = $this->redisService->delete($id);

        if (!$response) {
            throw new NotFoundHttpException(sprintf(
                'The resource \'%s\' was not found.',
                $id
            ));
        }

        $view = $this->view($response, Response::HTTP_OK);
        return $this->handleView($view);
    }
}
