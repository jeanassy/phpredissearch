<?php

namespace App\Services;

use Exception;
use Symfony\Component\Cache\Adapter\RedisAdapter;


/**
 * Service to connect to redis adapter
 * @author Jean Assy <info@jeanassy.com>
 */
class RedisService
{

    /**
     * @var RedisAdapter
     */
    private $redisClient;

    /**
     * @param string $redisUrl
     */
    public function __construct(string $redisUrl)
    {

        $this->redisClient = RedisAdapter::createConnection($redisUrl);

        $this->redisClient->setOption(\Redis::OPT_SERIALIZER, \Redis::SERIALIZER_PHP);
    }

    /**
     * @param array $keywords
     * 
     * @return array
     */
    public function search($keywords): array
    {
        $this->redisClient->setOption(\Redis::OPT_SCAN, \Redis::SCAN_RETRY);
        return $this->handleKeywords($keywords);
    }

    /**
     * @param string $document
     * @param int $id
     * 
     * @return array
     */
    public function create($id, $document): array
    {

        if (!empty($this->read($id))) {
            $message =  "Resource was already added.";
        } else {

            if ($this->redisClient->zadd('documents', $id, $document)) {
                $message =  "Resource was added successfully.";
            } else {
                $message =  "An error has occured.";
            }
        }
        return  ['response' => $message];
    }

    /**
     * @param int $id
     * 
     * @return array
     */
    public function read($id): array
    {
        $response = [];

        if ($result =  $this->redisClient->zRangeByScore("documents", $id, $id)) {
            $response = ["id" => $id, "document" => $result[0]];
        }

        return $response;
    }

    /**
     * @param int $id
     * 
     * @return bool
     */
    public function delete($id): bool
    {
        return $this->redisClient->zRemRangeByScore("documents", $id, $id);
    }

    /**
     * @param string $keyword
     * 
     * @return array
     */
    private function redisMatcher($keyword): array
    {

        $response = [];
        $it = null;

        while ($matches = $this->redisClient->zScan('documents', $it, "*{$keyword}*")) {
            foreach ($matches as  $id) {
                $response[] = ['id'=>$id];
            }
        }

        return $response;
    }


    /**
     * @param string $keywords
     * 
     * @return array
     */
    private function handleKeywords($keywords): array
    {
        $response = [];
        $keywords = explode(" ", $keywords);

        if (!empty($keywords)) {

            foreach ($keywords as $keyword) {
                $keyword = $this->cleanString($keyword);

                if (strlen($keyword) > 0) {
                    $data = $this->redisMatcher($keyword);
                    $response = array_merge($response, $data);
                }
            }
        }

        return $this->uniqueResult($response) ;
    }


    /**
     * @param string $keyword
     * 
     * @return string|null
     */
    private function cleanString($keyword)
    {

        $keyword = trim($keyword);
        $keyword = strip_tags($keyword);
        $keyword = htmlspecialchars($keyword, $ent = ENT_COMPAT, $charset = 'ISO-8859-1');

        return $keyword;
    }

    private function uniqueResult($array){

        $result = array_map("unserialize", array_unique(array_map("serialize", $array)));

        foreach ($result as $key => $value) {
            if (is_array($value)) {
                $result[$key] = $this->uniqueResult($value);
            }
        }
        return $result;

    }
}
